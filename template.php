<?php

function newsportal_primary_links() {
  $links = menu_primary_links();
  if ($links) {
    $output .= '<ul id="navlist">';
    foreach ($links as $link) {

      $output .= '<li>' . $link . '</li>';
    }; 
    $output .= '</ul>';
  }
  return $output;
}

function newsportal_secondary_links() {
  $links = menu_secondary_links();
  if ($links) {
  $output = implode("&nbsp;|&nbsp;",$links);
  }
  return $output;
}